#!/usr/bin/python

from __future__ import division

# Page size in [cm]
textwidth = 15.98915
textheigth = 24.76035

cm2in = 1/2.54
golden = 1.618034

figw = 0.48*textwidth*cm2in
figh = figw/golden

from matplotlib import rc
rc("text", usetex=True)
rc('font',**{'family':'serif','serif':['Times']})
rc("font", size=10)
rc("text", dvipnghack=True)  
rc("figure", figsize=(figw, figh))
#rc("figure.subplot", left=0.12, right=0.95, bottom=0.1, top=0.95)
rc("figure.subplot", left=0.15, right=0.95, bottom=0.2, top=0.95)
rc("axes", titlesize=10)
rc("legend", fontsize=10)

print "figw in cm: %.2f" %(figw/cm2in)
print "figh in cm: %.2f" %(figh/cm2in)
print "External phd_rc.py file did execute"
#fig.savefig("name.pdf", bbox_inches='tight', pad_inches=0, format="pdf")
#ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(300))

#ax4.legend(loc="lower left", ncol=3, mode="expand", borderaxespad=0, bbox_to_anchor=(0, 1, 1., 0.2))
