\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}					%for advanced mathmematical formulas
\usepackage{graphicx}					%always use this package if you want to insert graphics into your report
\usepackage{fullpage}



\graphicspath{{./graphs/rosenbrock/}{./graphs/quadric/}{./graphs/ackley/}{./graphs/rastrigin/}}


% Title Page
\title{\Huge{Particle Swarm Optimization Algorithm Task}\\[7cm]Advanced Design 814\\[2cm]}

\author{\Large Andreas Joss\\[0.5cm]16450612}
\date{\today}

\begin{document}
\maketitle


\newpage
\section{Introduction}
This report briefly explains the  particle swarm optimization algorithm. Furthermore, the algorithm is implemented in code and the results thereof are shown and discussed.

\section{Basic Theory}
The particle swarm optimization (PSO) algorithm essentially mimics the behaviour of a flock of creatures, such as birds or fish. It is therefore a population based algorithm. Consider a swarm consisting of $p$ particles randomly placed placed in an $n$-dimensional space. The behaviour of a such a swarm, the manner in which it changes its position, can be categorized by two main properties. Firstly, a swarm tends to move into the direction of the overall best location experienced by a particle of the swarm; for example, in nature this could very well be an area of promising food prospects. Thus it is assumed that particles share information amongst each other, which is referred to as `collaborative searching' [1]. Another assumption is that the surrounding region of these promising locations could uncover even more promising prospects [1]. Secondly, a particle of a swarm can explore by itself, which could possibly result in the discovery of a new best location. The latter is referred to as `cognitive searching'.

Mathematically, the positions of each particle is updated using the 'velocity rule' [1]. The performance of each particle's current location is evaluated using a cost function which is defined by the user of the algorithm. As previously mentioned, two types of searching occur within this algorithm. However it is important to note that each particle has a memory, which remembers the individual particle's best performance, and the swarm's best performance from the beginning of time up to the latest moment. 

\section{Implementation}
There are mainly two different versions of the `velocity update rule' to consider; `linear velocity update rule` and `classical velocity update rule' [1]. The only difference lies within the variables that produce randomness into the resulting velocity vector [1]. The latter, is the rule that is implemented for the experiments documented within this report.
Additional parameters which can be used to increase the performance of the algorithm \textit{for a particular solution} are disabled since the given task is to construct a `vanilla' flavour algorithm in code. Popular additional parameters include maximum velocity limits, position limits and inertia weight adjustment during the execution of iterations [1].

The `classical velocity update rule', uses two coefficients, $c_{1}$ and $c_{2}$ which represents the weighting factor for a particle to be biased towards cognitive and social learning respectively. The variables $\textbf{r}_{1k}^{i}$ and $\textbf{r}_{2k}^{i}$ are random vectors, meaning they have a random magnitude within $[0,1]$ and a random direction in an $n$-dimensional space. The vectors $p_{k}^{i}$ and $p_{k}^{g}$ represent the particle's memory of its own best position and the best position that the swarm has experienced respectively. The variable $\textbf{x}_{k}^{i}$ represents the particle's current position in the $n$-dimensional space. It is assumed that all particle's start with a zero velocity vector, that is:
\begin{equation}
 \textbf{v}_{0}^{i} = \textbf{0}
\end{equation}

The classical velocity update rule is described by the \textit{stochastic vector} $\textbf{\textit{v}}_{k}^{i}$ as:
\begin{equation}
  \textbf{\textit{v}}_{k}^{i} = c_{1} \textbf{r}_{1k}^{i} \circ (\textbf{p}_{k}^{i} - \textbf{x}_{k}^{i}) + c_{2} \textbf{r}_{2k}^{i} \circ (\textbf{p}_{k}^{g} - \textbf{x}_{k}^{i})
\end{equation}
where $\circ$ indicates element-by-element multiplication of the vectors.

The new velocity vector $\textbf{v}_{k+1}^{i}$ is calculated as follows:
\begin{equation}
 \textbf{v}_{k+1}^{i} = w\textbf{v}_{k}^{i} + \textbf{\textit{v}}_{k}^{i}
\end{equation}
where $w$ represents the inertia factor of a particle's movement.

The new position vector of the particles is then calculated as follows:
\begin{equation} \label{eq:4}
 \textbf{x}_{k+1}^{i} = \textbf{x}_{k}^{i} + \textbf{v}_{k+1}^{i}
\end{equation}
 
Equation (\ref{eq:4}) might look confusing at first glance, but nevertheless, time is introduced in the form of a psuedo-normalized unit time equivalent. Thus multiplication of $\textbf{v}_{k+1}^{i}$ with a unit number will again result in the vector $\textbf{v}_{k+1}^{i}$.

\section{Example Functions}
Example functions are used to evaluate the effectiveness of this particular implementation of the PSO algorithm. Each graph displays the output of the function for ten independent runs, each with the equal amount of iterations executed. Several parameters for the optimization is shown on the graph figures. The average of the ten independent results are shown on the graph, as well as the best result found considering all ten independent runs. All values of $f(\textbf{x})$ is displayed on a logarithmic scale since the values can initially be very large. The particles are given initial positional values that fit within the boundaries of $\pm2.048$, $\pm100$, $\pm30$ $\pm5.12$ respectively for each example function. Thereafter particles are left unbounded and are allowed to move out of these boundaries.


Four functions are used to evaluate the effectiveness of the PSO algorithm. The mathematical description of each function is given in this section.
\subsection{Rosenbrock Function}
 \begin{equation}
  f_{0}(x)=\sum_{i=1}^{n/2} (100(x_{2i} - x_{2i-1}^{2})^{2} + (1-x_{2i-1})^{2})
 \end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{1}$ which gives $f(\textbf{1}) = 0$
\subsection{Quadric Function}
\begin{equation}
 f_{1}(x)=\sum_{i=1}^{n}\left(\sum_{j=1}^{i} x_{j} \right)^{2}
\end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{0}$ which gives $f(\textbf{0}) = 0$
\subsection{Ackley Function}
\begin{equation}
 f_{2}(x)=-20\text{exp}\left(-0.2\sqrt{1/n \sum_{i=1}^{n} x_{i}^{2}}\right) -\text{exp}\left(1/n \sum_{i=1}^{n} \cos{(2\pi x_{i}})\right) + 20 + e
\end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{0}$ which gives $f(\textbf{0}) = 0$
\subsection{Rastrigin Function}
\begin{equation}
 f_{3}(x)=\sum_{i=1}^{n} (x_{i}^{2} - 10 \cos{(2\pi x_{i})} + 10)
\end{equation}
\\[0.5cm]
The theoretical solution is at $\textbf{x} = \textbf{0}$ which gives $f(\textbf{0}) = 0$

\section{Results}
The PSO algorithm is programmed in the Python language, due to future work which will also require extensive Python scripting. The Matplotlib library is used to display the results in a neat and informative manner.

\newpage
\subsection{Rosenbrock Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rosenbrock1.pdf} 
 \caption{Rosenbrock function optimized using 10 dimensions, population of 20, and 200 iterations}
 \label{fig:rosenbrock1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rosenbrock2.pdf} 
 \caption{Rosenbrock function optimized using 20 dimensions, population of 40, and 200 iterations}
 \label{fig:rosenbrock2}
\end{figure}

Figure \ref{fig:rosenbrock1} shows that this function converges quite easily with the parameters specified. Note how large the initial $f(\textbf{x})$ is during the first iterations.

Figure \ref{fig:rosenbrock2} indicates that the function takes longer to converge compared to Figure \ref{fig:rosenbrock1}, since 20 dimensions are being optimized. A greater population is used to help solve a problem with a larger number of dimensions. Also the cost function value $f(\textbf{x})$ increases to a less optimal value compared to the theoretical answer.

\newpage

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rosenbrock3.pdf} 
 \caption{Rosenbrock function optimized using 20 dimensions, population of 40, and 400 iterations}
 \label{fig:rosenbrock3}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rosenbrock4.pdf} 
 \caption{Rosenbrock function optimized using 30 dimensions, population of 50, and 400 iterations}
 \label{fig:rosenbrock4}
\end{figure}

Figure \ref{fig:rosenbrock3} shows that even when the number of iterations is increased as compared to the results obtained in Figure \ref{fig:rosenbrock2}, the algorithm struggles to find the global minimum.

In Figure \ref{fig:rosenbrock4} the dimensions and population size are increased as compared to Figure \ref{fig:rosenbrock3}. It is clear that the increase in variables increases the difficulty for the algorithm to find a global minimum, as can be seen by an even less optimum $f(\textbf{x})$. The values of $f(\textbf{x})$ also require more iterations to converge to some local minimum as compared to Figure \ref{fig:rosenbrock3}.

\newpage
\subsection{Quadric Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Quadric1.pdf} 
 \caption{Quadric function optimized using 10 dimensions, population of 20, and 200 iterations}
 \label{fig:quadric1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Quadric2.pdf} 
 \caption{Quadric function optimized using 10 dimensions, population of 40, and 200 iterations}
 \label{fig:quadric2}
\end{figure}

From Figure \ref{fig:quadric1} it is clear that this function is more difficult for the PSO algorithm to solve, considering the same parameters as are used in Figure \ref{fig:rosenbrock1}. The function value $f(\textbf{x})$ converges quite far from the theoretical value $f(\textbf{0}) = 0$.

In Figure \ref{fig:quadric2} the population size is increased while keeping all the other parameters identical to that of Figure \ref{fig:quadric1}. It is clear that the extra diversity in the population helps to significantly reduce the value of $f(\textbf{x})$.

\newpage
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Quadric4.pdf} 
 \caption{Quadric function optimized using 10 dimensions, population of 40, and 800 iterations}
 \label{fig:quadric3}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Quadric5.pdf} 
 \caption{Quadric function optimized using 20 dimensions, population of 50, and 800 iterations}
 \label{fig:quadric4}
\end{figure}

The number of iterations are increased in Figure \ref{fig:quadric3} while other parameters are kept identical to that of Figure \ref{fig:quadric2}. The function value $f(\textbf{x})$ reaches a much lower value than is obtained in 
Figure \ref{fig:quadric2}.

From \ref{fig:quadric4} it can be noted that the increase in dimensions has made it considerably harder for the PSO algorithm to solve this function.

\newpage
\subsection{Ackley Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Ackley7.pdf} 
 \caption{Ackley function optimized using 10 dimensions, population of 40, and 200 iterations}
 \label{fig:ackley1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Ackley4.pdf} 
 \caption{Ackley function optimized using 10 dimensions, population of 30, and 200 iterations}
 \label{fig:ackley2}
\end{figure}

Figure \ref{fig:ackley1} shows an independent run that did in fact find the theoretical answer of $f(\textbf{0}) = 0$, and that some other independent runs converges close to the theoretical answer. The remaining independent runs converge at another local minimum far from the global minimum. 

In Figure \ref{fig:ackley2}, the the inertia factor $w$ is slightly increased as compared to Figure \ref{fig:ackley1}. It seems that this example problem is very sensitive to an increase of the inertia factor $w$, and consequently the algorithm overshoots the position vectors which would lead to the global minimum. 

\newpage
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Ackley8.pdf} 
 \caption{Ackley function optimized using 20 dimensions, population of 40, and 200 iterations}
 \label{fig:ackley3}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Ackley10.pdf} 
 \caption{Ackley function optimized using 30 dimensions, population of 50, and 800 iterations}
 \label{fig:ackley4}
\end{figure}

In Figure \ref{fig:ackley3} the number of dimensions are increased. As a result, the PSO algorithm struggles more to find the global minimum.

As shown in Figure \ref{fig:ackley4} the dimensions are increased even further. To help the algorithm find a better solution, the population and number of iterations are increased. The optimum value found for $f(\textbf{x})$ is slightly worse as compared to Figure \ref{fig:ackley3}.

\newpage
\subsection{Rastrigin Function}
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rastrigin1.pdf} 
 \caption{Rastrigin function optimized using 10 dimensions, population of 20, and 200 iterations}
 \label{fig:rastrigin1}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rastrigin3.pdf} 
 \caption{Rastrigin function optimized using 10 dimensions, population of 50, and 200 iterations}
 \label{fig:rastrigin2}
\end{figure}

Figure \ref{fig:rastrigin1} shows that the values of $f(\textbf{x})$ converge slightly towards the minimum but settles at some local minimum compared to the global minimum $f(\textbf{0}) = 0$.

From Figure \ref{fig:rastrigin2} it seems that an increase in population size as compared to Figure \ref{fig:rastrigin1} helps to find improved solutions. 

\newpage
\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rastrigin4.pdf} 
 \caption{Rastrigin function optimized using 20 dimensions, population of 50, and 200 iterations}
 \label{fig:rastrigin3}
\end{figure}

\begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{Rastrigin6.pdf} 
 \caption{Rastrigin function optimized using 20 dimensions, population of 50, and 200 iterations}
 \label{fig:rastrigin4}
\end{figure}

In Figure \ref{fig:rastrigin3} it can be seen that an increase in dimensions drastically reduces the quality of the solution. 

Most parameters are kept identical for Figure \ref{fig:rastrigin4} as compared to Figure \ref{fig:rastrigin3}, except for the inertia factor. It is clear that the PSO algorithm applied to the Rastrigin function is very sensitive to a slight increase of the inertia factor $w$. Consequently the solution seems to diverge.

\newpage
\section{Conclusion}
From the results it seems that the PSO algorithm is a handy tool to use when a cost function of 10-20 variables need to be optimized. Some of the example functions in this report, such as the Quadric function are evidently more difficult for the PSO algorithm to solve than for instance the Rosenbrock function. Reasonable solutions could probably still be obtained for a 10-20 dimensional problem but it does not seem that this algorithm would be sufficient for a 30 dimensional problem. It is also noted that an increase of the population size seems to result in better solutions or at least faster convergence to some local minimums. Furthermore, it is interesting to note how sensitive the example problems and the PSO algorithm is toward the inertia factor. A slight increase in the inertia factor often resulted in a diverging solution.

All considered the PSO algorithm is relatively easy to implement and is worth a try to solve a multi-dimensional optimization problem.

\section{Bibliography}
1. Wilke DN, Kok S, Groenwold AA. Comparison of linear and classical velocity update rules in particle swarm optimization: notes on diversity. International Journal for Numerical Methods in Engineering 2006; 70:962-984.


\end{document}          
