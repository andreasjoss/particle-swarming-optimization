#!/usr/bin/python
#This script is based on the Particle Swarming algorithm which is used to approximate the discrete global optimum.

#Author		Andreas Joss
#Student no	16450612
#Date		2015/03/04
#Task 		Advanced Design 814

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import argparse
import pylab as pl
import os
##########################################################################################
total_runs = 0
pop = 0                  		#population size
n_dim = 0                 		#number of dimensions
##########################################################################################

parser = argparse.ArgumentParser(description="Particle Swarming Algorithm",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument("choice", help="Select example function to test the PSO algorithm on:\n"+
		    "1 - Rosenbrock function\n"+
		    "2 - Quadric function\n"+
		    "3 - Ackley function\n"+
		    "4 - Rastrigin function\n\n", type=int, choices=[1,2,3,4])					#add a new POSITIONAL argument with a help message included

parser.add_argument("i", help="Enter total number of iterations the algorithm should complete", type=int)	#add a new POSITIONAL argument with a help message included
parser.add_argument("p", help="Enter desired population the algorithm should use", type=int)			#add a new POSITIONAL argument with a help message included
parser.add_argument("n", help="Enter number of dimensions the example problem should have", type=int)		#add a new POSITIONAL argument with a help message included
#parser.add.argument("filename", help="Enter the name of the file the output figure should be saved to", type=argparse.FileType('w'))

parser.add_argument("-a","--average", default=1, help="Enter total number of independant runs to calculate average solution", type=int)	#add a new OPTIONAL argument with a help message included

args = parser.parse_args()													#store the argument parsed into the variable "args"

if args.i != None:
  total_runs = args.i
  
if args.p != None:
  pop = args.p
  
if args.n != None:
  n_dim = args.n

#TEST_PARAMETERS
##########################################################################################
#1 Rosenbrock function
if args.choice == 1:
  upperbound = 2.048
  lowerbound = -2.048
  example_string = "Rosenbrock"

#2 Quadric function
elif args.choice == 2:
  upperbound = 100
  lowerbound = -100
  example_string = "Quadric"

#3 Ackley function
elif args.choice == 3:
  upperbound = 30
  lowerbound = -30
  example_string = "Ackley"

#4 Rastrigin function
elif args.choice == 4:
  upperbound = 5.12
  lowerbound = -5.12
  example_string = "Rastrigin"
##########################################################################################

num_attributes = 1			#number of attributes for each creature (only the value f(xk) need to be stored, unlike GA where fitness was also stored)
k = 0					#normalized unit pseudotime increment
i = 0					#particle i
g = 0					#index number of group's best performing particle
c1 = 2.5				#cognitive scaling factor (weight of own best experience)
c2 = 2					#social scaling factor (weight of group's best experience)
j = 0					#just a counter
bounded = False

w = 0.40				#inertia factor (any value between 0.4 to 0.9 is typically used) although values up to 1.2 can also be used
					#large w -->	good for the beginning phase of iterations. Enables a particle to move quickly to the estimated location of the solution
					#		bad if value to large near completion of algorithm, as it causes excessive overshoot and algorithm becomes similiar to random number generator
					
					#small w -->	good for near completion of optimization process when refinement of solution is needed
					#		bad if value too small in beginning of process, as swarm/flock can become stuck at local minima or even experience a "premature convergence"
								
best_fx = []
avg_fx = []
w_arr = []
temp = pl.np.zeros((pop,1))
percentage_complete = 0
independant_runs = 0
best_avg = 0
best_ever = 0
great_total_counter = 0
example_temp = pl.np.zeros((n_dim+1,1))	#serves as the array which will deal with indexing problem (Artinetively, I could also add another attribute column which could serve as x[0]) 

#append empty list to existing best_fx[] list (thus constructing a 2D list)
for i in range (0,args.average):
  best_fx.append([])

##########################################################################################
#print test TEST_PARAMETERS
print "The following example function will be tested : " + example_string
print "Total number of iterations:	%d" %total_runs
print "Population size:		%d" %pop
print "Dimensions:			%d" %n_dim
print "Independant Runs:		%d\n" %args.average

#TEST_FUNCTIONS
#1 Rosenbrock function
def rosenbrock(x):
  sum = 0
  for i in range (1,int(n_dim/2)+1):		#NB, sum of boundary includes i = n_dim/2
    sum = sum + pl.np.power(100*(x[2*i]-pl.np.power(x[2*i-1],2)),2) + pl.np.power((1-x[2*i-1]),2)
  return sum

def quadric(x):
  sum = 0
  sum_inner = 0
  for i in range (1,n_dim+1):			#NB, sum of boundary includes i = n_dim
    for j in range (1,i+1):
      sum_inner = sum_inner + x[j]		#NB, sum of boundary includes j = i
    sum = sum + pl.np.power(sum_inner,2)
  return sum

def ackley(x):
  sumA = 0
  sumB = 0
  for i in range (1,n_dim+1):			#NB, sum of boundary includes i = n_dim
    sumA = sumA + pl.np.power(x[i],2)
    sumB = sumB + pl.np.cos(2*pl.np.pi*x[i])
    
  return -20*pl.np.exp(-0.2*pl.np.sqrt((1/n_dim)*sumA)) - pl.np.exp((1/n_dim)*sumB) + 20 + pl.np.exp(1)

def rastrigin(x):
  sum = 0
  for i in range (1,n_dim+1):			#NB, sum of boundary includes i = n_dim
    sum = sum + (pl.np.power(x[i],2) - (10*pl.np.cos(2*pl.np.pi*x[i])) + 10)
  return sum

def example(x):
  #these examples require the first array index to be referred as 1 (which Matlab does, but Python refers to the first index as 0)
  #for this reason a temporary array is created to deal with this issue
  for i in range (1,n_dim+1):
    example_temp[i] = x[i-1]
  
  if args.choice == 1:
    return rosenbrock(example_temp)
  elif args.choice == 2:
    return quadric(example_temp)
  elif args.choice == 3:
    return ackley(example_temp)
  elif args.choice == 4:
    return rastrigin(example_temp)

##########################################################################################
for independant_runs in range (0,args.average):
  r1k		= pl.np.random.uniform(low=0,high=1,size = (pop,1))				#random vector with each component a random magnitude between 0 and 1
  r2k		= pl.np.random.uniform(low=0,high=1,size = (pop,1))				#random vector with each component a random magnitude between 0 and 1
  xk		= pl.np.random.uniform(low=lowerbound,high=upperbound,size = (pop,n_dim))	#initial position is a random vector with each component a random magnitude between 0 and 1
  vk		= pl.np.zeros((pop,n_dim))							#initial vector velocities of each particle i is given as 0
  vk_stoch	= pl.np.zeros((pop,n_dim))	
  f_pk		= pl.np.zeros((pop,1))						
  fk		= pl.np.zeros((pop,args.average),dtype=pl.np.float32)
  pk		= xk  
  
  while k < total_runs:
    great_total_counter = great_total_counter + 1
    print pl.np.round((great_total_counter/(total_runs*args.average))*100,0)," percent complete     \r",	#The comma prevents print from adding a newline. (and the spaces will keep the line clear from prior output)
									#Also, don't forget to terminate with a print "" to get at least a finalizing newline!
    k = k + 1
  
    for i in range (0,pop):          	#run trough population
      fk[i,independant_runs] = example(xk[i,:])
    temp = pl.np.sort(fk[:,independant_runs],axis=0)
    temp = pl.np.reshape(temp,(pop,1))
    best_fx[independant_runs].append(temp[0])
    #best_fx.append(temp[0])
  
    if k==1 and independant_runs==0:
      best_ever = temp[0]
      
    elif temp[0]<best_ever:
      best_ever = temp[0]
  
    #Calculate f(xk) (particle's previous best score) and the position thereof for each particle, and determine group's best particle (g) and the position thereof
    sum = 0
    for i in range (0,pop):          	#run trough population
      fk[i,independant_runs] = example(xk[i,:])
      sum = sum + fk[i,independant_runs]    
      if k == 1:
	f_pk[i] = fk[i,independant_runs]
	pk[i,:] = xk[i,:]
      elif fk[i,independant_runs] < f_pk[i]:		#compare each particle's current performance with its previous best performance
	f_pk[i] = fk[i,independant_runs]			#record f values for each particle that improved
	pk[i,:] = xk[i,:]			#record the corresponding position values for each particle that improved
      if i == 0 and k == 1:
	g = i
      elif fk[i,independant_runs] < fk[g,independant_runs]:
	g = i
    
    r1k = pl.np.random.uniform(low=0,high=1,size = (pop,n_dim))						#random vector with each component a random magnitude between 0 and 1
    r2k = pl.np.random.uniform(low=0,high=1,size = (pop,n_dim))						#random vector with each component a random magnitude between 0 and 1

    for i in range (0,pop):										
      vk_stoch[i,:] = c1*r1k[i,:]*(pk[i,:]-xk[i,:]) + c2*r2k[i,:]*(pk[g,:]-xk[i,:])				#classical pso velocity update rule (stochastic velocity vector v_stoch)
      vk[i,:] = w*vk[i,:] + vk_stoch[i,:]									#update velocity vector
      xk[i,:] = xk[i,:] + vk[i,:]										#position vector for particle i in the swarm, updated for each new k iteration
    
    if bounded == True:  
      for i in range (0,pop):
	for j in range (0,n_dim):
	  if xk[i,j] > upperbound:
	    xk[i,j] = upperbound
	  elif xk[i,j] < lowerbound:
	    xk[i,j] = lowerbound


  temp = pl.np.sort(fk[:,independant_runs],axis=0)
  temp = pl.np.reshape(temp,(pop,1))
  #best_fx[independant_runs].append(temp[0])
  best_avg = best_avg + temp[0]
  k=0
  
best_avg = best_avg/args.average

print "Average solution of independant runs f(x) = %.2f\n\nBest solution of independant runs    f(x) = %.2f" %(best_avg,best_ever)

  
#Maximize output window
'''
### for 'TkAgg' backend5
pl.switch_backend('TkAgg')
print '#1 Backend:',pl.get_backend()
figManager = pl.get_current_fig_manager()
figManager.resize(*figManager.window.maxsize())			#linux
#figManager.window.state('zoomed') 				#works fine on Windows!	
'''
'''
### for 'Qt4Agg' backend
pl.switch_backend('QT4Agg')
print '#2 Backend:',pl.get_backend()
figManager = pl.get_current_fig_manager()
figManager.window.showMaximized()
'''
#os.system("./phd_rc.py")

'''
#Direct input 
pl.rcParams['text.latex.preamble']=[r"\usepackage{lmodern}"]
#Options
params = {'text.usetex' : True,
          'font.size' : 11,
          'font.family' : 'lmodern',
          'text.latex.unicode': True,
          }
pl.rcParams.update(params)
'''

fig1 = pl.figure(1)
pl.title("f(x) vs. number of iterations executed")

pl.xlabel("Iteration number")
pl.ylabel("f(x)")
pl.yscale('log')
for i in range (0,args.average):
  pl.plot(range(0,total_runs),best_fx[i], label = "Run %d: Optimum f(x)" %(i+1))

pl.figtext(0.40,0.882, "Total iterations: %d\nPopulation: %d\nDimensions: %d\nInertia factor: %.2f\nC1: %.1f\nC2: %.1f" %(total_runs,pop,n_dim,w,c1,c2),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'left',verticalalignment='top')
l = pl.legend(loc=1)# draggable(state=True))
l.draggable(state=True)
pl.figtext(0.1375,0.2375, "Average result of independant runs\nf(x) = %.2f\n\nBest result of independant runs\nf(x) = %.2f" %(best_avg,best_ever),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'left',verticalalignment='top')

params = {'legend.fontsize': 10 ,
          'legend.linewidth': 2,
          'text.usetex' : False}
          #'figure.dpi': 80,
          #'savefig.dpi': 80}
pl.rcParams.update(params)

F = pl.gcf()
figure_dpi = F.get_dpi()
figure_size = F.get_size_inches()
print "figure_dpi: %d" %figure_dpi
print "figure_size_cms: ", figure_size*2.54
print "Thus image should be a %i x %i Image" %(figure_dpi*figure_size[0], figure_dpi*figure_size[1])
F.savefig("./graphs/rastrigin/Rastrigin9.pdf",bbox_inches='tight',format='PDF',pad_inches=0.1)

pl.show()


#params = {'legend.fontsize': 16 ,
#          'legend.linewidth': 2,
#          'figure.dpi': 150}			#figure.dpi: 80 lyk great op show()
          #'savefig.dpi': 100}
#pl.rcParams.update(params)			#Then do the plot afterwards. There are a ton of other rcParams, they can also be set in the matplotlibrc file.

#savefig("test.png", dpi=gcf().dpi)
#show()

#pl.savefig("pso_results.png", dpi=300)#, facecolor='w', edgecolor='w',
        #format=None,transparent=False, bbox_inches=None, pad_inches=0.1,
        #frameon=None)
#pl.savefig("pso_results.pdf", dpi=80, facecolor='w', edgecolor='w',format="pdf",pad_inches=0)
        #orientation='landscape', papertype=None, format=None,
        #transparent=False, bbox_inches=None, pad_inches=0.1,
        #frameon=None)

#pl.savefig("pso_results.pdf", bbox_inches='tight', pad_inches=0, format="pdf")
#pl.savefig("pso_results.png")

#print "Fig1.dpi : %d" %fig1.dpi


#pl.savefig("pso_results.pdf", facecolor='w', edgecolor='w',format="pdf", bbox_inches='tight',pad_inches=0)

#alpha -- float (0.0 transparent through 1.0 opaque)
#bbox -- rectangle prop dict
#fontsize -- a float in points