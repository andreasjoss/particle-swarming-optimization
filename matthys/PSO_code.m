%..................Advanced Design - Assignment 2........................//
%% Author: MPI du Toit
%Student number: 15714241
%Date: 23 Feb. 2015
%% ...................Partical Swarm Algorithm...........................//
%%
% Inproduction: 
%   �Would you tell me, please, which way I ought to go from here?�
%   �That depends a good deal on where you want to get to,� said the Cat.
%   �I don�t much care where�� said Alice.
%   �Then it doesn�t matter which way you go,� said the Cat.
%              � Alice�s Adventures in Wonderland, by Lewis Carroll (1865).

% The steps:
% 1.) Initialization    - creating initial population and hyperspace 
% 2.) Evaluation        - Evaluate function values  
% 3.) Modify            - Modify previous and global best
% 4.) Terminate         - Terminate on some condition
% 5.) Repeat loop       - Go to step 2 

% This editon of my code is optimizing the Ackley function.

clear all
clc
%% 1.) Initialization
% Setting parameters
D = 30;             % Max number of dimensions
P = 200;            % Max number of particles
N = 1000;           % Max number of iterations
phiU = 32.768;      % Function upper limit
phiL = -32.768;     % Function lower limit
w = 0.4;            % Inertia
c1 = 2;             % Cognative scaling factor
c2 = 2;             % Social scaling factor

% Initializing the population
X = rand(P,D).*(phiU - phiL)+phiL;
Vp = (0.5 - rand(P,D)).*(phiU-phiL);
i = 1;
while i <= P;       % Calculating function values and particle best
    F(i) = -20*exp(-0.2*sqrt((1/D)*sum(X(i,:).^2)))-exp((1/D)*sum(cos(2*pi.*X(i,:))))+20+exp(1);
    Fbest(i) = F(i);   
    Pbest(i,:) = X(i,:);     % Storing initial particle best
    i = i+1;
end
   

[Fgbest,Ele] = min(F);       % Calculating global best
Gbest = X(Ele,:);

%% 2.) Evaluation 
% Initialize itterations
t = 0;
while t<=N
       
i = 1;

while i<=P                  % Loop for stepping though particles
    r1 = rand(1,D);
    r2 = rand(1,D);    
    j=1;
    while j<=D              % Loop for stepping through dimansions
        V(i,j) = w.*Vp(i,j)+c1*r1(1,j).*(Pbest(i,j) - X(i,j)) + c2*r2(1,j).*(Gbest(1,j) - X(i,j));
        Xn(i,j) = X(i,j) + V(i,j);
        j = j+1;
    end
    i = i+1;
end

X = Xn;                     % Store calculated values in global matrix
Vp = V;
 
i = 1;
while i<=P;                 % Calculate new function values for particles
    F(i) = -20*exp(-0.2*sqrt((1/D)*sum(X(i,:).^2)))-exp((1/D)*sum(cos(2*pi.*X(i,:))))+20+exp(1);
    i = i+1;
end

%% 3.) Modify
i = 1;
while i<=P                  % Modify the particle best
if F(i) <= Fbest(i)
    Fbest(i) = F(i);
    Pbest(i,:) = X(i,:);
end
    
if F(i) <= Fgbest           % Modify the global best
    Fgbest = F(i);
    Gbest(1,:) = X(i,:);
end

i = i+1;

end

%% 4.) Terminate or 5.) Repeat loop

t = t+1;
end

